<?php

Route::get('hello-world', function(){
	echo 'Hello from the package!';
});

Route::get('add/{a}/{b}', 'Devtdung\Helloworld\HelloworldController@add');
Route::get('subtract/{a}/{b}', 'Devtdung\Helloworld\HelloworldController@subtract');