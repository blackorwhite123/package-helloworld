<?php

namespace Devtdung\Helloworld;

use Illuminate\Support\ServiceProvider;

class HelloworldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Devtdung\Helloworld\HelloworldController');
        $this->loadViewsFrom(__DIR__.'/views', 'helloworld');
    }
}
