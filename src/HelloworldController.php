<?php

namespace Devtdung\Helloworld;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelloworldController extends Controller
{
     public function add($a, $b){
    	$result =  $a + $b;
    	return view('helloworld::add', compact('result'));
    }

    public function subtract($a, $b){
    	echo $a - $b;
    }
}
